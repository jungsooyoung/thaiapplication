package com.example.lg.thaiapplication;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.DefaultItemAnimator;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ProgressBar;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import android.os.AsyncTask;
import android.widget.Toast;

import com.example.lg.thaiapplication.Adapters.RecyclerViewAdapter;
import com.example.lg.thaiapplication.InterFace.OnLoadMoreListener;
import com.example.lg.thaiapplication.InterFace.OnTaskCompleted;
import com.example.lg.thaiapplication.ModelClasses.PropertyRecyclerModel;
import com.example.lg.thaiapplication.Network.ListNetworkActivity;

public class MainActivity extends AppCompatActivity implements OnTaskCompleted {
    private ArrayList<PropertyRecyclerModel> RowListModelClassArrayList;    //Listview에 List생성하려면, 각각의 Row에 대한 Model도 Array로 가지고 있어야 한다.
    private RecyclerView recyclerView;
    private RecyclerViewAdapter recyclerViewAdapter;

    private ListNetworkActivity network;
    private RvRestApiThread RAT;
    private LinearLayoutManager mLayoutManager;
    protected Handler handler;
    public static int pageNumber;   //page번호

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        pageNumber = 1;
        InitWiget();

        recyclerViewAdapter = new RecyclerViewAdapter(MainActivity.this, RowListModelClassArrayList, recyclerView);   //어뎁터생성 ( 등록시킬화면, 데이터형식이 담긴 배열)
        recyclerView.setAdapter(recyclerViewAdapter);
        getWebServiceData();

        recyclerViewAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                RowListModelClassArrayList.add(null);
                recyclerViewAdapter.notifyItemInserted(RowListModelClassArrayList.size()-1);
                ++pageNumber;
                getWebServiceData();
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
            try {
                Class.forName("android.os.AsyncTask");
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        CardViewClickSupport.addTo(recyclerView).setOnItemClickListener(new CardViewClickSupport.OnItemClickListener() {        //RecyclerView에 Item 짧게 눌렀을때
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                //Toast.makeText(MainActivity.this, "클릭한 아이템은 " + RowListModelClassArrayList.get(position).getPropertyTitle(), Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
                intent.putExtra("propertyId", RowListModelClassArrayList.get(position).getPropertyId().toString());
                startActivity(intent);
            }
        });

        CardViewClickSupport.addTo(recyclerView).setOnItemLongClickListener(new CardViewClickSupport.OnItemLongClickListener() {     //RecyclerView Item 길게 눌렀을때,
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                //Toast.makeText(MainActivity.this, "길게누름" + RowListModelClassArrayList.get(position).getPropertyTitle(), Toast.LENGTH_SHORT).show();
                return true;
            }
        });
        /******************  recyclerView 에 onScrollListner 달아줄 필요없다. (Adapter에서 등록함)  *******************************/
    }

    private void InitWiget(){
        MainActivity.this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);   //activity_main.xml 의 recyclerView (xml 의 객체화)4
        mLayoutManager = new LinearLayoutManager(MainActivity.this);    //RecyclerView 형식의 LayoutManager 생성 (화면에 Layout을 등록해 줄 객체)
        recyclerView.setLayoutManager(mLayoutManager);  //RecyclerView에 레이아웃매니저 지정.
        RowListModelClassArrayList = new ArrayList<>();         // 1. Model을 담을 ArrayList 생성. ArrayList<PropertyRecyclerModel>
        handler = new Handler();
        network = new ListNetworkActivity();
    }

    public void getWebServiceData() {
        RAT = new RvRestApiThread(this,this);      //AsyncTask객체는 오직 1번만 실행할 수 있기때문에 매번 객체를 생성해서 가져온다.
        RAT.execute();
    }


    @Override
    public void onTaskCompleted(JSONObject response) {

        parsejosnData(response);

    }

    public void parsejosnData(JSONObject response){
        try{

            JSONObject jsondata = response;  // 서버에서 받은 JSON Data
            JSONArray mJsonArray = (JSONArray)jsondata.getJSONObject("result").getJSONArray("cardList");

            if(RowListModelClassArrayList.size() > 0) {
                RowListModelClassArrayList.remove(RowListModelClassArrayList.size() - 1);
                recyclerViewAdapter.notifyItemRemoved(RowListModelClassArrayList.size());
            }

            for (int i = 0; i < mJsonArray.length() ; i++) {
                JSONObject temp = mJsonArray.getJSONObject(i);
                String id = temp.getString("id");
                String title = temp.getString("title");
                String comment1 = temp.getString("comment1");
                String comment2 = temp.getString("comment2");
                String mainPrice = temp.getString("mainPrice");
                String img = temp.getString("img");
                ArrayList<String> tagList = new ArrayList<>();

                for(int j=0; j < temp.getJSONArray("tagList").length(); j++){
                    tagList.add(temp.getJSONArray("tagList").get(j).toString());
                }

                PropertyRecyclerModel property = new PropertyRecyclerModel(id,title,comment1,comment2,mainPrice,img,tagList);
                RowListModelClassArrayList.add(property);  // ArrayList<PropertyRecyclerModel> 에 추가

                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        recyclerViewAdapter.notifyItemInserted(recyclerView.getChildCount());
                    }
                });
            }
            recyclerViewAdapter.setLoaded();

        }
        catch (JSONException e){
            e.printStackTrace();
        }
    }



    class RvRestApiThread extends AsyncTask<Void, JSONObject, JSONObject>{  // 순서대로 doBackground(인자1),  onProgressUpdate(인자2), doBackground의 리턴값


        ProgressDialog dialog;
        public OnTaskCompleted listener = null;//Call back interface
        Context context;


        public RvRestApiThread(Context context1, OnTaskCompleted listener1) {
            context = context1;
            listener = listener1;   //Assigning call back interface  through constructor
        }

        @Override
        protected void onPreExecute() { //네트워크작업, 객체생성작업
                dialog = new ProgressDialog(MainActivity.this);
                dialog.setMessage("Loading, please wait");
                dialog.setTitle("Connecting server");
                dialog.show();
                dialog.setCancelable(false);

            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(Void ... params){
            synchronized (this) {
                JSONObject jsondata;  // 서버에서 받은 JSON Data
                jsondata = network.getJSonObject();
                //publishProgress(jsondata); //중간 중간 진행상태를 UI에 Update 하도록  @Override protected void onProgressUpdate(JSONObject... params) { 호출

                return jsondata;
            }

        }

        @Override
        protected void onPostExecute(JSONObject result) {
            dialog.cancel();
            listener.onTaskCompleted(result);
        }

    }



}
