package com.example.lg.thaiapplication.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.support.v7.widget.RecyclerView;

import com.example.lg.thaiapplication.InterFace.OnLoadMoreListener;
import com.example.lg.thaiapplication.R;
import com.example.lg.thaiapplication.ModelClasses.PropertyRecyclerModel;        //Model Import 되어야 됨.
import com.example.lg.thaiapplication.customfonts.MyTextView_Roboto_Regular;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static com.example.lg.thaiapplication.R.drawable.facilities_rect;

/* <어댑터 기능>
* 1. view , viewholder 생성
* 2. item과 viewholder를 bind
* 3.  RecyclerView에게 change를 감지(notify)
* 4.  아이템클릭등의 interaction handling
* 5.  viewTypes 분기
* 6.  onFailedToRecyclerView를 통해 RecyclerView를 복원
* */

public class RecyclerViewAdapter extends RecyclerView.Adapter/*<RecyclerViewAdapter.RecyclerViewHolder>*/ {
    private final int VIEW_ITEM = 1;    //Item형태의 ViewType
    private final int VIEW_PROG = 0;    //progressbar형태의 View Type

    Context context;
    private List<PropertyRecyclerModel> OfferList;
    private LinearLayout dynamicContainer;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    //  1. 아이템 View를 저장하는 ViewHolder 생성.

    public RecyclerViewAdapter(Context context, List<PropertyRecyclerModel> list, RecyclerView recyclerView){     // 1: 나타낼 화면, 2 : rowlist의 데이터모델
        //MainActivity에서 데이터 리스트를 넘겨 받는다.
        this.OfferList = list;
        this.context = context;     // 넘어온 context 는 MainActivity 의 context ( = MainActivty.this )

        if(recyclerView.getLayoutManager() instanceof LinearLayoutManager){ //recyclerView의 LayoutManager 가 LinearLayoutManager 라면,
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager)recyclerView.getLayoutManager();

            recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    super.onScrolled(recyclerView, dx, dy);
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();

                    if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                        // End has been reached
                        // Do something
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        loading = true;
                    }

                }
            });
        }

    }

    public class RecyclerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {   // ViewHolder' 란, 이름 그대로 뷰들을 홀더에 꼽아놓듯이 보관하는 객체
                                                                                                             // 각각의 Row를 그려낼 때 그 안의 위젯들의 속성을 변경하기 위해 findViewById를 호출하는데 이것의 비용이 큰것을 줄이기 위해 사용합니다.
        TextView propertyID, propertyTitle, propertyComment1, propertyComment2, propertyMainprice;
        ArrayList<TextView> propertyTaglist;            //ArrayList가 아니고 , View의 배열 형식이 되어야 되고 , 동적으로 생성되어야 한다.
        ImageView propertymainimage;

        public RecyclerViewHolder(View view) {        //보니까, ( onCreateViewHolder 호출 -> MyViewHolder 생성 먼저 하고 -> onBindViewHolder 하고 ) 이걸 list하나당 계속 반복함..
            super(view);

            propertyID=(TextView) view.findViewById(R.id.PropertyID);
            propertyTitle=(TextView)view.findViewById(R.id.PropertyTitle);
            propertyComment1=(TextView)view.findViewById(R.id.PropertyComment1);
            propertyComment2=(TextView) view.findViewById(R.id.PropertyComment2);
            propertyMainprice=(TextView) view.findViewById(R.id.PropertyMainprice);
            dynamicContainer = (LinearLayout)view.findViewById(R.id.TaglistLayout);    //TagList 붙일 Layout (동적 추가)
            propertyTaglist = new ArrayList<TextView>();    // onBindViewHolder 호출

            propertymainimage = (ImageView) view.findViewById(R.id.PropertyMainImage);  //main Image

            view.setOnClickListener(this);  //클릭 이벤트 담당
        }

        public void onClick(View view){ //CardView 하나 클릭시 반응할 이벤트 선언
            int adapterPos = getAdapterPosition();  //ViewHolder가 나타내는 항목의 어댑터 위치를 리턴한다. 이를 이용하면 원하는 position의 아이템을 찾아서 처리할 수 있겠다!
        }

    }

    @Override   //필수구현 : ItemView를 위한 뷰홀더 객체를 생성하여 리턴
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {  //viewType 형태의 아이템 뷰를 위한 뷰홀더 객체 생성.
        /*
        * (parent == RecyclerView가 나오는 ViewGroup == 상위xml인 activty_main.xml)
        * */
        RecyclerView.ViewHolder vh;

        if(viewType == VIEW_ITEM){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_listview, parent, false);

            vh = new RecyclerViewHolder(v);

        }else{
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_item, parent, false);

            vh = new ProgressViewHolder(v);
        }
        //View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_listview, parent, false); // 상위xml인 activty_main View에 row_listview View를 parsing해서 가져와 하나하나 붙인다.

        return vh;  //즉, row_listview.xml 을 ViewGroup 의 형태로 리턴! !
    }

    @Override
    public int getItemCount() {
        return OfferList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {       //position에 해당하는 데이터를 뷰홀더의 아이템뷰에 표시. (for문)

        if(holder instanceof RecyclerViewHolder) {
            PropertyRecyclerModel lists = OfferList.get(position);

            ((RecyclerViewHolder)holder).propertyID.setText(lists.getPropertyId());
            ((RecyclerViewHolder)holder).propertyTitle.setText(lists.getPropertyTitle());
            ((RecyclerViewHolder)holder).propertyComment1.setText(lists.getPropertyComment1());
            ((RecyclerViewHolder)holder).propertyComment2.setText(lists.getPropertyComment2());
            ((RecyclerViewHolder)holder).propertyMainprice.setText(lists.getPropertyMainPrice());

            for (int i = 0; i < lists.getPropertyTagList().size(); i++) {
                TextView temp = (TextView) new TextView(context);   //context 는 MainActivity.this 와 같다. 이게 아니고, 왠지 rowlist_detail.xml 의 context가 되어야 될 것 같은 느낌... 모르겠다 함 해보자
                temp.setText(lists.getPropertyTagList().get(i).toString());
                temp.setTextColor(Color.BLUE);
                temp.setBackgroundDrawable(ContextCompat.getDrawable(context, facilities_rect));

                temp.setPadding(7, 5, 7, 5);
                LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                lp.gravity = Gravity.CENTER;
                lp.leftMargin = 10;
                temp.setLayoutParams(lp);

                ((RecyclerViewHolder)holder).propertyTaglist.add(temp);
                dynamicContainer.addView(temp);
            }

            if (lists.getPropertyImgSrc() != null && !lists.getPropertyImgSrc().isEmpty()) {     //Main Image 달기
                Picasso.get().load(lists.getPropertyImgSrc().toString()).into(((RecyclerViewHolder)holder).propertymainimage);
            }
        }
        else if(holder instanceof ProgressViewHolder){
            ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
        }

    }


    /*  progressbar를 static method로 선언한 이유 (객체마다 다른 동작이 아닌, 하나의 일련된 동작이라는 점과 모든 객체가 공유해야 하는 위젯이기 때문 */
    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
        }
    }

    @Override
    public int getItemViewType(int position) {  //position에 값 있으면 RecyclerView , 없으면 ProgressBar
        return OfferList.get(position) != null ? VIEW_ITEM : VIEW_PROG;
    }

    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }

    public void setLoaded() {
        loading = false;
    }


}
