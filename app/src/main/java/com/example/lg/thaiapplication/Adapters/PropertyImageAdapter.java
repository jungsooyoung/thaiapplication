package com.example.lg.thaiapplication.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.lg.thaiapplication.MainActivity;
import com.example.lg.thaiapplication.ModelClasses.PropertyImageModel;
import com.example.lg.thaiapplication.ModelClasses.PropertyRecyclerModel;
import com.example.lg.thaiapplication.R;

import java.util.List;

public class PropertyImageAdapter extends RecyclerView.Adapter<PropertyImageAdapter.MyViewHolder> {

    Context context;
    private List<PropertyImageModel> OfferList;  //전체 이미지 모델을 가지고 있을 List

    public class MyViewHolder extends RecyclerView.ViewHolder {     //이미지 잡고있을 홀더
        ImageView image;
        TextView imageNo;

        public MyViewHolder(View view){
            super(view);

            image = (ImageView)view.findViewById(R.id.image);
            imageNo = (TextView)view.findViewById(R.id.imageNo);
        }
    }

    public PropertyImageAdapter(Context mainActivityContacts, List<PropertyImageModel> offerList){ // 이미지 장착해줄 어뎁터
        this.OfferList = OfferList;
        this.context = mainActivityContacts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){ //parent == property_images가 나오는 ViewGroup
        View itemView = LayoutInflater.from(parent.getContext())      // Property 이미지를 담는 상위 xml의 ViewGroup을 가져와서
                .inflate(R.layout.property_images, parent, false);  // 그 상위 xml의 ViewGroup에서 property_images 를 parsing해서 가져온다. ( parameter = resoruce, ViewGroup root, boolean AttchToRoot)

        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position){
        PropertyImageModel lists = OfferList.get(position);
        holder.image.setImageResource(lists.getImage());
        holder.imageNo.setText(lists.getImageNo());
    }

    @Override
    public int getItemCount() {
        return OfferList.size();

    }


}
