package com.example.lg.thaiapplication.Network;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ListNetworkActivity extends AppCompatActivity  {
    public static int ItemCursor = 5; // 가져오려는 Item Number 1 => 5 => 9 =>
    public static int ItemLimit = 4; // 가져오려는 Item Number 4
    final static String RESTAPIURL = "http://www.homethai.ga/api/v1.0/cards?";  //"http://133.186.132.21/api/v1.0/posts?cursor=10&limit=10";
                                                                                          //http://www.homethai.ga/api/v1.0/cards?cursor=5&limit=4
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public JSONObject getJSonObject(){  //시작 지점
        JSONObject jsondata;
        try {
            String tempurl = getURL();
            System.out.println(tempurl);
            URL url = new URL(tempurl.toString());
            HttpURLConnection urlConnection;
            urlConnection = (HttpURLConnection) url.openConnection();
            /*
            urlConnection.setReadTimeout(3000);
            urlConnection.setConnectTimeout(3000);
            urlConnection.setDoOutput(true);
            urlConnection.setDoInput(true);
            */
            urlConnection.setRequestMethod("GET");
            urlConnection.setUseCaches(false);

            int responseStatusCode = urlConnection.getResponseCode();

            InputStream inputStream;
            if (responseStatusCode == HttpURLConnection.HTTP_OK) {
                inputStream = urlConnection.getInputStream();
            } else {
                inputStream = urlConnection.getErrorStream();
            }
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            jsondata = new JSONObject(getStringFromInputStream(in));
        }catch (MalformedURLException e){
            System.err.println("Malformed URL");
            e.printStackTrace();
            return null;
        }catch (JSONException e){
            System.err.println("JSON parsing error");
            e.printStackTrace();
            return null;
        }catch(IOException e){
            System.err.println("URL Connection failed");
            e.printStackTrace();
            return null;
        }
        return jsondata;
    }

    private String getURL(){
        String url = RESTAPIURL; //cursor=10&limit=10
        url += "cursor=" + ItemCursor;
        url += "&limit=" + ItemLimit;
        ItemCursor += ItemLimit; //가져오려는 위치더해주기
        return url;
    }

    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }
}
