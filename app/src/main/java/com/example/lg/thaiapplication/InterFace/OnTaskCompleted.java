package com.example.lg.thaiapplication.InterFace;

import org.json.JSONObject;

public interface OnTaskCompleted {
    void onTaskCompleted(JSONObject response);
}
