package com.example.lg.thaiapplication;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.lg.thaiapplication.Adapters.MyCustomPagerAdapter;
import com.example.lg.thaiapplication.ModelClasses.PropertyRecyclerDetailModel;
import com.example.lg.thaiapplication.Network.DetailNetworkActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.lg.thaiapplication.R.drawable.facilities_rect;
import com.viewpagerindicator.CirclePageIndicator;

public class DetailsActivity extends AppCompatActivity /*implements OnMapReadyCallback*/{
    private ViewPager viewPager;
    ArrayList<View> imageViews;
    MyCustomPagerAdapter myCustomPagerAdapter;
    PropertyRecyclerDetailModel detailModel;
    private DetailNetworkActivity network;
    private DetailsActivity.RestApiThread RAT;
    private TextView propertyId,propertyTitle ,propertyComment1 ,propertyComment2, propertyMainPrice; //, propertyTaglist, propertyImageSrc;
    private TextView imageNo;
    String propertytid;
    private LinearLayout dynamicContainer;
    CirclePageIndicator indicator;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.rowlist_detail);

        this.imageViews = new ArrayList<>();
        this.viewPager = (ViewPager)findViewById(R.id.viewpager);
        this.indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        this.imageNo = (TextView)findViewById(R.id.imageNo);
        this.propertyId = (TextView)findViewById(R.id.PropertyID);
        this.propertyTitle = (TextView)findViewById(R.id.PropertyTitle);
        this.propertyComment1 = (TextView)findViewById(R.id.PropertyComment1);
        this.propertyComment2 = (TextView)findViewById(R.id.PropertyComment2);
        this.propertyMainPrice = (TextView)findViewById(R.id.PropertyMainPrice);
//        this.propertyTaglist = (TextView)findViewById(R.id.PropertyTaglist);
        this.dynamicContainer = (LinearLayout)findViewById(R.id.TaglistLayout);

        Intent intent = getIntent();
        propertytid = intent.getExtras().getString("propertyId");

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                imageNo.setText("" + (position +1));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        if(network == null){
            network = new DetailNetworkActivity();
        }

        RAT = new RestApiThread();  //restapi 통신 객체 생성
        RAT.execute();  //통신 ㄱㄱ
    }

    class RestApiThread extends AsyncTask<Void, JSONObject, Integer> {  // 순서대로 doBackground(인자1),  onProgressUpdate(인자2), doBackground의 리턴값
        @Override
        protected void onPreExecute() { //네트워크작업, 객체생성작업
            if(RAT == null) {
                RAT = new DetailsActivity.RestApiThread();
                System.out.println("첫 Details RAT 객체 생성" );
            }else{
                System.out.println("Details RAT 객체가 있어 새로 생성하지 않습니다." );
            }

            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Void ... params){
            System.out.println("Details doInBackground 함수 안입니다.");

            JSONObject jsondata;  // 서버에서 받은 JSON Data
            jsondata = network.getJSonObject(propertytid);
            System.out.println("Id 값이 "+ propertytid.toString() + "인 Details jsondata 출력!! : " +  jsondata );
            publishProgress(jsondata); //중간 중간 진행상태를 UI에 Update 하도록
            return 0;
        }

        @Override
        protected void onProgressUpdate(JSONObject... params) {
            System.out.println("Details onProgressUpdate 함수 안입니다.");
            try {
                String cardId = params[0].getJSONObject("result").getString("cardId");
                String cardDetailId = params[0].getJSONObject("result").getString("cardDetailId");
                String title = params[0].getJSONObject("result").getString("title");
                String comment1 = params[0].getJSONObject("result").getString("comment1");
                String comment2 = params[0].getJSONObject("result").getString("comment2");
                String mainPrice = params[0].getJSONObject("result").getString("mainPrice");
                String description = params[0].getJSONObject("result").getString("description");
                JSONArray tagList = params[0].getJSONObject("result").getJSONArray("tagList");

                ArrayList<String> taglist = new ArrayList();
                for(int i=0;  i < tagList.length(); i++){
                    taglist.add(tagList.getString(i));

                    TextView temp = (TextView) new TextView(DetailsActivity.this);   //context 는 MainActivity.this 와 같다. 이게 아니고, 왠지 rowlist_detail.xml 의 context가 되어야 될 것 같은 느낌... 모르겠다 함 해보자
                    temp.setText(tagList.getString(i));
                    temp.setTextColor(Color.BLUE);
                    temp.setBackgroundDrawable(ContextCompat.getDrawable(DetailsActivity.this, facilities_rect) );

                    temp.setPadding(7,5,7,5);
                    LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    lp.gravity = Gravity.CENTER;
                    lp.leftMargin = 10;
                    temp.setLayoutParams(lp);

                    dynamicContainer.addView(temp);
                }

                JSONArray imgList = params[0].getJSONObject("result").getJSONArray("imgList");  //imageList를 받아옴.
                for(int i=0;  i < imgList.length(); i++){
                    ImageView imageView = new ImageView(DetailsActivity.this);

                    if(imgList.getString(i) != null && ! imgList.getString(i).isEmpty()) {     //Image 달기
                        Glide.with(DetailsActivity.this).load(imgList.getString(i)).into(imageView);
                        imageViews.add(imageView);
                        //imageViewArrayListUrl.add(imgList.getString(i).toString());
                    }
                }

                myCustomPagerAdapter = new MyCustomPagerAdapter(DetailsActivity.this, imageViews);      //서버에서 받아와 , 등록할 이미지를 등록한다.
                viewPager.setAdapter(myCustomPagerAdapter);

                indicator.setViewPager(viewPager);
                final float density = getResources().getDisplayMetrics().density;
                indicator.setRadius(5 * density);
                NUM_PAGES = imageViews.size();

                final Handler handler = new Handler();
                final Runnable Update = new Runnable() {
                    public void run() {
                        if (currentPage == NUM_PAGES) {
                            currentPage = 0;
                        }
                        viewPager.setCurrentItem(currentPage++, true);
                    }
                };

                Timer swipeTimer = new Timer();
                swipeTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        handler.post(Update);
                    }
                }, 3000, 3000);

                myCustomPagerAdapter.notifyDataSetChanged();

                String phoneNumber = params[0].getJSONObject("result").getString("phoneNumber");

                detailModel = new PropertyRecyclerDetailModel(cardId , cardDetailId, title, comment1, comment2, mainPrice, description, imageViews, taglist, phoneNumber);     //데이터 생성

            }catch (JSONException e){
                System.err.println("  Details onProgressUpdate 안에서 error ");
                e.printStackTrace();
            }


        }
        @Override
        protected void onPostExecute(Integer result) {
            System.out.println("Details onPostExecute 함수 안입니다.");

            propertyTitle.setText(detailModel.getPropertyTitle());
            propertyComment1.setText(detailModel.getPropertyComment1());
            propertyComment2.setText(detailModel.getPropertyComment2());
            propertyMainPrice.setText(detailModel.getPropertyMainPrice());

            super.onPostExecute(result);

        }
    }

}
