package com.example.lg.thaiapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by sooyoung on 2019-03-01
 */
public class SplashActivity extends AppCompatActivity {
    /** 로딩 화면이 떠있는 시간(밀리초단위)  **/
    private final int SPLASH_DISPLAY_LENGTH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.first_splash);
        StartLoading();
    }

    private void StartLoading(){
        Handler hd = new Handler();
        hd.postDelayed(new splashhandler(), SPLASH_DISPLAY_LENGTH); //3초 후에 hd Handler 실행.
    }

    private class splashhandler implements Runnable{
        public void run(){
            startActivity( new Intent(getApplication(), MainActivity.class) ); //로딩이 끝난후 이동할 Activity
            SplashActivity.this.finish();   //로딩 페이지에서 Activitiy Stack에서 제거
        }
    }
}