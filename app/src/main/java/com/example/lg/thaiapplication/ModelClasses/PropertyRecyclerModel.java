package com.example.lg.thaiapplication.ModelClasses;

import java.util.ArrayList;

public class PropertyRecyclerModel {
    private String propertyId,propertyTitle,propertyComment1,propertyComment2,propertyMainPrice,propertyImgSrc;
    private ArrayList<String> propertyTagList;

    public PropertyRecyclerModel(String propertyId, String propertyTitle, String propertyComment1, String propertyComment2, String propertyMainPrice, String propertyImgSrc, ArrayList<String> propertyTagList) {
        this.propertyId = propertyId;
        this.propertyTitle = propertyTitle;
        this.propertyComment1 = propertyComment1;
        this.propertyComment2 = propertyComment2;
        this.propertyMainPrice = propertyMainPrice;
        this.propertyImgSrc = propertyImgSrc;
        this.propertyTagList = propertyTagList;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public String getPropertyTitle() {
        return propertyTitle;
    }

    public String getPropertyComment1() {
        return propertyComment1;
    }

    public String getPropertyComment2() {
        return propertyComment2;
    }

    public String getPropertyMainPrice() {
        return propertyMainPrice;
    }

    public String getPropertyImgSrc() {
        return propertyImgSrc;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public void setPropertyTitle(String propertyTitle) {
        this.propertyTitle = propertyTitle;
    }

    public void setPropertyComment1(String propertyComment1) {
        this.propertyComment1 = propertyComment1;
    }

    public void setPropertyComment2(String propertyComment2) {
        this.propertyComment2 = propertyComment2;
    }

    public void setPropertyMainPrice(String propertyMainPrice) {
        this.propertyMainPrice = propertyMainPrice;
    }

    public void setPropertyImgSrc(String propertyImgSrc) {
        this.propertyImgSrc = propertyImgSrc;
    }

    public void setPropertyTagList(ArrayList<String> propertyTagList) {
        this.propertyTagList = propertyTagList;
    }

    public ArrayList<String> getPropertyTagList() {

        return propertyTagList;
    }
}
