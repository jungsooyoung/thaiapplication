package com.example.lg.thaiapplication.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.lg.thaiapplication.R;
import com.squareup.picasso.Picasso;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class MyCustomPagerAdapter extends PagerAdapter {

    Context context;
    ArrayList<String> imageViewArrayList;
    List<View> views;
    LayoutInflater layoutInflater;  //XML에 정의된 Resource(자원) 들을 View의 형태로 반환해 줌

    public MyCustomPagerAdapter(Context context, List<View> views){ //어뎁터 생성자 사용시 Context 필수
        this.context = context;
        this.views = views;
        //this.imageViewArrayList = imagesUrl;
        //layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);    // inflate 를 사용하기 위해서는 우선 inflater 를 얻어온다. (레이아웃 XML 파일을 View객체로 만듬.)
        layoutInflater = LayoutInflater.from(context);
    }

    public View getView(int position){
        return views.get(position);
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View view , Object object){     // instantiateItem 에서 생성한 객체를 사용할지 여부를 판단.
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container , int position ,Object object){
        container.removeView(views.get(position));
    }

    @Override
    public Object instantiateItem(ViewGroup container , int position){
        View view = views.get(position);
        container.addView(view);

        return view;    //장착한 itemView 를 리턴.
    }

    @Override
    public int getItemPosition(Object object) {
        for(int index = 0 ; index < getCount() ; index++){
            if((View)object == views.get(index)) {
                return index;
            }
        }
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return "View " + (position + 1);
    }


}
