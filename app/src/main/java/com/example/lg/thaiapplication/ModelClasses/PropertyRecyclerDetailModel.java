package com.example.lg.thaiapplication.ModelClasses;

import android.view.View;

import java.util.ArrayList;

public class PropertyRecyclerDetailModel {  //상세 화면
    private String propertyId, propertyDetailId, propertyTitle, propertyComment1, propertyComment2, propertyMainPrice, propertyDescription;
    private String propertyPhoneNm;
    private ArrayList<View> propertyImgList;
    private ArrayList<String> propertyTagList;

    public PropertyRecyclerDetailModel(String propertyId, String propertyDetailId, String propertyTitle, String propertyComment1, String propertyComment2, String propertyMainPrice, String propertyDescription, ArrayList<View> propertyImgList, ArrayList<String> propertyTagList, String propertyPhoneNm) {
        this.propertyId = propertyId;
        this.propertyDetailId = propertyDetailId;
        this.propertyTitle = propertyTitle;
        this.propertyComment1 = propertyComment1;
        this.propertyComment2 = propertyComment2;
        this.propertyMainPrice = propertyMainPrice;
        this.propertyPhoneNm = propertyPhoneNm;
        this.propertyDescription = propertyDescription;
        this.propertyImgList = propertyImgList;
        this.propertyTagList = propertyTagList;
    }

    public void setPropertyDetailId(String propertyDetailId) {
        this.propertyDetailId = propertyDetailId;
    }

    public String getPropertyDetailId() {
        return propertyDetailId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public void setPropertyTitle(String propertyTitle) {
        this.propertyTitle = propertyTitle;
    }

    public void setPropertyComment1(String propertyComment1) {
        this.propertyComment1 = propertyComment1;
    }

    public void setPropertyComment2(String propertyComment2) {
        this.propertyComment2 = propertyComment2;
    }

    public void setPropertyMainPrice(String propertyMainPrice) {
        this.propertyMainPrice = propertyMainPrice;
    }

    public void setPropertyPhoneNm(String propertyPhoneNm) {
        this.propertyPhoneNm = propertyPhoneNm;
    }

    public void setPropertyImgList(ArrayList<View> propertyImgList) {
        this.propertyImgList = propertyImgList;
    }

    public void setPropertyTagList(ArrayList<String> propertyTagList) {
        this.propertyTagList = propertyTagList;
    }

    public String getPropertyId() {
        return propertyId;
    }

    public String getPropertyTitle() {
        return propertyTitle;
    }

    public String getPropertyComment1() {
        return propertyComment1;
    }

    public String getPropertyComment2() {
        return propertyComment2;
    }

    public String getPropertyMainPrice() {
        return propertyMainPrice;
    }

    public String getPropertyPhoneNm() {
        return propertyPhoneNm;
    }

    public ArrayList<View> getPropertyImgList() {
        return propertyImgList;
    }

    public ArrayList<String> getPropertyTagList() {
        return propertyTagList;
    }
}
